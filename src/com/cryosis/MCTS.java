package com.cryosis;

import java.util.Collections;
import java.util.Comparator;

public class MCTS {

    public static int search(Game game) {
        final int player = game.getPlayer();

        Node node = new Node(game);

//        final long end = System.currentTimeMillis() + 500;
//        while (System.currentTimeMillis() < end) {
        int counter = 0;
        while (counter++ < 1000) {

//            while (node.fullyExpanded()) {
            while (!node.isLeaf()) {
                    node = node.moveDown(UCT.findBestNodeWithUCT(node));
            }

            if (node.getVisits() != 0) {
                node.expandNode();
                if (node.hasChildren())
                    node = node.moveDown(0);
            }

            // Roll Out
            final int score = rolloutNode(node, player);

            // Back Propagate
            node.addScore(score);
            node.incrementVisits();
            while (node.hasParent()) {
                node = node.moveUp();
                node.addScore(score);
                node.incrementVisits();
            }
        }

        return Collections.max(node.getChildren(), Comparator.comparing(Node::getVisits)).getMove();
    }

    private static int rolloutNode(Node node, int rootPlayer) {
        int startingDepth = node.getDepth();

        while (!node.isTerminal()) {
            final Node child = node.addRandomChild();
            node = node.moveDown(child);
        }

        int score = node.getGame().getScore(rootPlayer);
        while (node.getDepth() != startingDepth) {
            assert node.getParent().getChildren().size() == 1;
            node.getParent().getChildren().clear();
            node = node.moveUp();
        }

        return score;
    }
}
