package com.cryosis;

public class Main {

    public static void main(String[] args) {
            GameImpl game = new GameImpl();

            int debugCounter = 0;
            while (!game.isOver()) {
                final int move = MCTS.search(game);
                game.playMove(move);

                System.out.println(String.format("Monte Carlo Suggests %s for player %s", move, game.getPlayer()));
                System.out.println(game.toString());

                game.togglePlayer();
        }
    }
}