package com.cryosis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GameImpl implements Game {

    public static final int PLAYER_1 = 1;
    public static final int PLAYER_2 = 2;

    private int boardSize;
    private int[] board;
    private int currentPlayer;

    public GameImpl() {
        this(PLAYER_1, 3);
    }

    public GameImpl(int player) {
        this(player, 3);
    }

    public GameImpl(int currentPlayer, int boardSize) {
        this.currentPlayer = currentPlayer;
        this.boardSize = boardSize;
        this.board = new int[boardSize * boardSize];
    }

    @Override
    public List<Integer> getPossibleActions() {
        List<Integer> possibleActions = new ArrayList<>();
        for (int i = 0; i < board.length; i++) {
            if (board[i] == 0)
                possibleActions.add(i);
        }
        return possibleActions;
    }

    @Override
    public boolean isOver() {
        return hasWon(PLAYER_1) || hasWon(PLAYER_2) || Arrays.stream(board).noneMatch(value -> value == 0);
    }

    @Override
    public void togglePlayer() {
        currentPlayer = 3 - currentPlayer;
    }

    @Override
    public int getPlayer() {
        return currentPlayer;
    }

    @Override
    public int getScore(int rootPlayer) {
        assert isOver();

        if (hasWon(rootPlayer))
            return 1;
        else if (hasWon(3 - rootPlayer))
            return -1;
        else return 0;
    }

    @Override
    public void playMove(int move) {
        board[move] = currentPlayer;
    }

    @Override
    public void clearMove(int move) {
        board[move] = 0;
    }

    private boolean hasWon(int player) {
        return (
                player == board[0] && player == board[1] && player == board[2] ||
                        player == board[3] && player == board[4] && player == board[5] ||
                        player == board[6] && player == board[7] && player == board[8] ||
                        player == board[0] && player == board[3] && player == board[6] ||
                        player == board[1] && player == board[4] && player == board[7] ||
                        player == board[2] && player == board[5] && player == board[8] ||
                        player == board[0] && player == board[4] && player == board[8] ||
                        player == board[2] && player == board[4] && player == board[6]
        );
    }

    @Override
    public String toString() {
        final List<java.lang.String> boardStrings = Arrays.stream(board).mapToObj(value -> value == 0 ? " " : value == 1 ? "X" : "O").collect(Collectors.toList());
        StringBuilder sb = new StringBuilder()
                .append(" " + boardStrings.get(0) + " | " + boardStrings.get(1) + " | " + boardStrings.get(2) + "\n")
                .append("-----------\n")
                .append(" " + boardStrings.get(3) + " | " + boardStrings.get(4) + " | " + boardStrings.get(5) + "\n")
                .append("-----------\n")
                .append(" " + boardStrings.get(6) + " | " + boardStrings.get(7) + " | " + boardStrings.get(8) + "\n");
        return sb.toString();
    }

}
