package com.cryosis;

import java.util.List;

public interface Game {

    List<Integer> getPossibleActions();

    boolean isOver();

    void togglePlayer();

    int getPlayer();

    int getScore(int rootPlayer);

    void playMove(int move);

    void clearMove(int move);
}
