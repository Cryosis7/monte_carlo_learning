package com.cryosis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Node {
    private List<Node> children = new ArrayList<>();
    private Node parent = null;
    private int depth = 0;
    private int visits = 0;
    private int score = 0;
    private Game game;
    private int move = -1;

    public Node(int depth) {
        this.depth = depth;
    }

    public Node(Game game) {
        this.game = game;
    }

    public boolean isLeaf() {
        return children.size() == 0;
    }

    public boolean allChildrenExplored() {
        return children.size() > 0 && children.stream().noneMatch(node -> node.getVisits() == 0);
    }

    public Node addChild(int move) {
        Node child = new Node(depth + 1);
        child.setGame(game);
        child.setParent(this);
        child.setMove(move);
        children.add(child);
        return child;
    }

    public void expandNode() {
        game.getPossibleActions().forEach(this::addChild);
    }

    public boolean isTerminal() {
        return game.isOver();
    }

    public Node addRandomChild() {
        final List<Integer> possibleActions = game.getPossibleActions();
        final Integer randomMove = possibleActions.get(ThreadLocalRandom.current().nextInt(possibleActions.size()));

        return addChild(randomMove);
    }

    public void addScore(int score) {
        this.score = score;
    }

    public void incrementVisits() {
        visits++;
    }

    public Node moveDown(Node child) {
        assert children.contains(child);
        game.playMove(child.getMove());
        game.togglePlayer();
        return child;
    }

    public Node moveDown(int childIndex) {
        assert children.size() > childIndex;
        Node child = children.get(childIndex);
        game.playMove(child.getMove());
        game.togglePlayer();
        return child;
    }

    public Node moveUp() {
        assert parent != null;
        game.clearMove(move);
        game.togglePlayer();
        return parent;
    }

    public Node getChildWithNoVisits() {
        for (Node n : children)
            if (n.visits == 0)
                return n;

        return null;
    }

    // -------------------
    // Getters and Setters

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getDepth() {
        return depth;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public boolean hasParent() {
        return parent != null;
    }
}
